﻿using SofiaWebReportes.Filters;
using SofiaWebReportes.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SofiaWebReportes.Controllers
{
    [AllowCrossSiteJson]
    public class InformacionResumenActividadesController : ApiController
    {
        public IHttpActionResult GetNodos(Guid id, string parametros, int tipo, int tipotiempo, string fechainicio, string fechafinal)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var format = "dd/MM/yyyy";
            var t1 = DateTime.UtcNow;
            //tipo
            //1: Estudiante
            //2: Grupo
            //3: Escuela


            //tipo tiempo
            //1: Últimos 7 días
            //2: Últimos 15 días
            //3: Últimos mes
            //4: Últimos dos meses
            //5: Últimos seis meses
            //6: Último año
            //7: Histórico
            //8: Fecha específica

            var now = DateTime.UtcNow.Date;
            var dt_fechainicio =
                    tipotiempo == 1 ? now.AddDays(-7) :
                    tipotiempo == 2 ? now.AddDays(-15) :
                    tipotiempo == 3 ? now.AddMonths(-1) :
                    tipotiempo == 4 ? now.AddMonths(-2) :
                    tipotiempo == 5 ? now.AddMonths(-6) :
                    tipotiempo == 6 ? now.AddYears(-1) :
                    tipotiempo == 7 ? new DateTime(2013, 1, 1) : DateTime.ParseExact(fechainicio, format, provider);
            var dt_fechafinal = tipotiempo == 8 ? DateTime.ParseExact(fechafinal, format, provider) : now;


            IEnumerable<ViewModel_nodo> nodos = null;
            using (var client = new HttpClient())
            {
                var t2 = DateTime.UtcNow;
                if (tipo == 2) //grupo
                {
                    //cambiar
                    var c = Calculos.ObtenerResumenActividadesGrupo(id, dt_fechainicio, dt_fechafinal);
                    var t3 = DateTime.UtcNow;
                    c.TiempoConsultaNodos = (t2 - t1).Seconds;
                    c.TiempoCalculoNodos = (t3 - t2).Seconds;
                    c.TiempoRespuesta = (t3 - t1).Seconds;
                    return Ok(c);
                }
                if (tipo == 3) //escuela
                {
                    //cambiar
                    var c = Calculos.ObtenerResumenActividadesGrupo(id, dt_fechainicio, dt_fechafinal);
                    var t3 = DateTime.UtcNow;
                    c.TiempoConsultaNodos = (t2 - t1).Seconds;
                    c.TiempoCalculoNodos = (t3 - t2).Seconds;
                    c.TiempoRespuesta = (t3 - t1).Seconds;
                    return Ok(c);
                }
                

            }
            //var product = products.Where(p => p.Id == idprueba || p.Name.Contains(search));

            if (nodos == null)
            {
                return NotFound();
            }
            return Ok(nodos);
        }

    }
}
