﻿using SofiaWebReportes.Filters;
using SofiaWebReportes.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Http;

namespace SofiaWebReportes.Controllers
{
    [AllowCrossSiteJson]
    public class ReporteAcomuladoController : ApiController
    {
        public IHttpActionResult GetNodos(string FechaInicio, string FechaFinal, Guid? IdEstado,
            int? Dependencia, string Nombre, int? NivelEscolar, Guid? IdEscuela, Guid? IdDistribuidor, int? Order)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var format = "d/M/yyyy";
            var t1 = DateTime.UtcNow;

            var FechaIni = DateTime.ParseExact(FechaInicio, format, provider);
            var FechaFin = DateTime.ParseExact(FechaFinal, format, provider);


            var param_iddistribuidor = IdDistribuidor.HasValue ? IdDistribuidor.Value : Guid.Empty;
            var param_idescuela = IdEscuela.HasValue ? IdEscuela.Value : Guid.Empty;
            var param_idestado = IdEstado.HasValue ? IdEstado.Value : Guid.Empty;
            var param_fechainicio = FechaIni;
            var param_fechafinal = FechaFin;
            var param_dependencia = Dependencia.HasValue ? (Dependencia.Value == 0 ? -1 : Dependencia.Value) : -1;
            var param_nombreescuela = !string.IsNullOrEmpty(Nombre) ? Nombre : "";
            var param_nivelescolar = NivelEscolar.HasValue ? (NivelEscolar.Value == 0 ? -1 : NivelEscolar.Value) : -1;
            var param_order = Order.HasValue ? Order.Value : -1;

            using (math_reportesEntities db = new math_reportesEntities())
            {
                var reporte = db.sofia_reporte_escuela_acomulado(param_fechainicio, param_fechafinal, param_idestado, param_dependencia, param_nivelescolar, param_nombreescuela, param_idescuela, param_iddistribuidor, param_order).ToList();
                return Ok(reporte);
            }
        }
    }
}