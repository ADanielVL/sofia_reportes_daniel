﻿using SofiaWebReportes.Filters;
using SofiaWebReportes.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SofiaWebReportes.Controllers
{
    [AllowCrossSiteJson]
    public class InformacionResumenEjerciciosController : ApiController
    {
        public IHttpActionResult GetNodos(Guid id,  int tipo, int tipotiempo,
           string fechainicio, string fechafinal)
        {
            
            CultureInfo provider = CultureInfo.InvariantCulture;
            var format = "d/M/yyyy";
            var t1 = DateTime.UtcNow;
            //tipo
            //1: EjerciciosGrupo
            //2: EjerciciosEscuela
            //3:EjerciciosGruposEscuela
            //4:ActividadesGrupos

            //tipo tiempo
            //1: Últimos 7 días
            //2: Últimos 15 días
            //3: Últimos 30 días
            //4: Últimos 60 días
            //5: Últimos 6 meses
            //6: Último año
            //7: Histórico
            //8: Fecha específica
           // fechainicio = fechainicio+ " 00:00";
            var now = DateTime.UtcNow.Date;

            var FechaIni =
                    tipotiempo == 1 ? now.AddDays(-7) :
                    tipotiempo == 2 ? now.AddDays(-15) :
                    tipotiempo == 3 ? now.AddMonths(-1) :
                    tipotiempo == 4 ? now.AddMonths(-2) :
                    tipotiempo == 5 ? now.AddMonths(-6) :
                    tipotiempo == 6 ? now.AddYears(-1) :
                    tipotiempo == 7 ? new DateTime(2013, 1, 1) : DateTime.ParseExact(fechainicio, format, provider);

            var FechaFin = tipotiempo == 8 ? DateTime.ParseExact(fechafinal, format, provider) : now;

            using (math_reportesEntities db = new math_reportesEntities())
            {
            if (tipo == 1)
            {
                // sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result Actividades = new sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result();
                var Actividades = db.sofia_grupo_ObtenerInformacionEjercicios_ResumenDiarioAcumulado(FechaIni, FechaFin, id).ToList();
                //Actividades.
                return Ok(Actividades);
            }
                if (tipo==2)
                {
                    var Actividades = db.sofia_escuela_ObtenerInformacionEjercicios_ResumenDiarioAcumulado(FechaIni, FechaFin, id).ToList();
                    return Ok(Actividades);
                }
                if (tipo==3)
                {
                    // Group_math grupos = new Group_math();
                   List<sofia_grupo_ObtenerInformacionEjercicios_ResumenDiarioAcumulado_Result> Actividades = new List<sofia_grupo_ObtenerInformacionEjercicios_ResumenDiarioAcumulado_Result>();
                   var  grupos = db.Group_math.Where(x=> x.SchoolID ==id && x.IsActivo).ToList();
                    foreach (var g in grupos)
                    {
                        var Act = db.sofia_grupo_ObtenerInformacionEjercicios_ResumenDiarioAcumulado(FechaIni, FechaFin, g.GroupID).FirstOrDefault();
                        if (Act != null)
                        {
                            Actividades.Add(Act);
                        }
                       
                    }
                    
                    return Ok(Actividades);
                }
                if (tipo == 4)
                {
                    // Group_math grupos = new Group_math();
                    List<sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result> Actividades = new List<sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result>();
                    var grupos = db.Group_math.Where(x => x.SchoolID == id && x.IsActivo).ToList();
                    foreach (var g in grupos)
                    {
                        sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result Act = db.sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado(FechaIni, FechaFin, g.GroupID).FirstOrDefault();
                        Actividades.Add(Act);
                    }

                    return Ok(Actividades);
                }
                if (tipo == 5)
                {
                    // Group_math grupos = new Group_math();
                    List<sofia_escuela_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result> Actividades = new List<sofia_escuela_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result>();
                    
                  var Act = db.sofia_escuela_ObtenerInformacionActividades_ResumenDiarioAcumulado(FechaIni, FechaFin, id).ToList();
                    
                    return Ok(Act);
                }
                if (tipo == 6)
                {
                    // Group_math grupos = new Group_math();
                    List<sofia_estudiante_ObtenerInformacionEjercicios_ResumenDiarioAcumulado_Result> Actividades = new List<sofia_estudiante_ObtenerInformacionEjercicios_ResumenDiarioAcumulado_Result>();

                    var Act = db.sofia_estudiante_ObtenerInformacionEjercicios_ResumenDiarioAcumulado(FechaIni, FechaFin, id).ToList();

                    return Ok(Act);
                }
                if (tipo == 7)
                {
                    //Un solo grupo
                    List<sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result> Actividades = new List<sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result>();

                        var Act = db.sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado(FechaIni, FechaFin,id).FirstOrDefault();
                        Actividades.Add(Act);


                    return Ok(Actividades);
                }


                return Ok();
            }
        }

    }

}
