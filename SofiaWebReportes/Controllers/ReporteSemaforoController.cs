﻿using SofiaWebReportes.Filters;
using SofiaWebReportes.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Http;

namespace SofiaWebReportes.Controllers
{
    [AllowCrossSiteJson]
    public class ReporteSemaforoController : ApiController
    {

        public IHttpActionResult GetNodos(Guid? IdDistribuidor, string FechaInicio, string FechaFinal,
            int? Dependencia, string Ciudad, string CCT, string Nombre, int? NivelEscolar, int Tipo)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var format = "d/M/yyyy";
            var t1 = DateTime.UtcNow;

            var FechaIni = DateTime.ParseExact(FechaInicio, format, provider);
            var FechaFin = DateTime.ParseExact(FechaFinal, format, provider);


            var param_iddistribuidor = IdDistribuidor.HasValue ? IdDistribuidor.Value : Guid.Empty;
            var param_fechainicio = FechaIni;
            var param_fechafinal = FechaFin;
            var param_Ciudad = !string.IsNullOrEmpty(Ciudad) ? Ciudad : "";
            var param_dependencia = Dependencia.HasValue ? (Dependencia.Value == 0 ? -1 : Dependencia.Value) : -1;
            var param_nombreescuela = !string.IsNullOrEmpty(Nombre) ? Nombre : "";
            var param_CCT = !string.IsNullOrEmpty(CCT) ? CCT : "";
            var param_nivelescolar = NivelEscolar.HasValue ? (NivelEscolar.Value == 0 ? -1 : NivelEscolar.Value) : -1;
            
            using (math_reportesEntities db = new math_reportesEntities())
            {
                if(Tipo == 1)
                {
                    var reporte = db.sofia_reporte_semaforo(param_iddistribuidor, param_fechainicio, param_fechafinal, param_Ciudad, param_dependencia, param_nombreescuela, param_CCT, param_nivelescolar).ToList();

                    return Ok(reporte);
                }
                else if(Tipo == 2)
                {
                    var reporte = db.sofia_reporte_semaforo_Escuela(param_iddistribuidor, param_fechainicio, param_fechafinal, param_Ciudad, param_dependencia, param_nombreescuela, param_CCT, param_nivelescolar).ToList();

                    return Ok(reporte);
                }
                else if(Tipo == 3)
                {
                    var reporte = db.sofia_reporte_semaforo_individual(param_iddistribuidor, param_fechainicio, param_fechafinal).ToList();

                    return Ok(reporte);
                }
                else
                {
                    var reporte = db.sofia_reporte_profesor_semaforo(param_fechainicio, param_fechafinal, param_iddistribuidor).ToList();

                    return Ok(reporte);
                }
            }
        }
    }
}