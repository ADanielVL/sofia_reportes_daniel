﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;


namespace SofiaWebReportes.Models
{
    public static class Calculos
    {
       
        #region Resumen conceptos con lista de parametros

        public static ViewModel_resultados_ejercicios CalcularInformacionEstudianteConcepto_ResumenAcumulado(List<ViewModel_nodo> ListaNodos, Guid IdEstudiante,  DateTime fechainicio, DateTime fechafinal )
        {
            var r = new sofia_estudiante_ObtenerInformacionListaConceptos_ResumenDiarioAcumulado_Result();
            //var r1 = new sofia_general_ObtenerInformacionConceptos_ResumenDiario_Result();

            using (var db = new math_reportesEntities())
            {
                fechafinal = db.Log_procesamiento.Where(x => x.Tipo == 8).Max(x => x.FechaProcesadaInicio);
            }

            fechainicio = fechainicio > fechafinal ? fechafinal : fechainicio;

            var l_conceptos = string.Join(",", ListaNodos.Select(x => x.properties.uuid.ToString()));


            using (var db = new math_reportesEntities())
            {
                r = db.sofia_estudiante_ObtenerInformacionListaConceptos_ResumenDiarioAcumulado(fechainicio,fechafinal,IdEstudiante, l_conceptos).FirstOrDefault();
                //r1 = db.sofia_general_ObtenerInformacionConceptos_ResumenDiario(fechainicio, fechafinal, l_conceptos).FirstOrDefault();
            }


            //var p = r1.PromedioCalificacion;
            //var ran_1 = r1.PromedioCalificacion - r1.MitadDesviacionCalificacion - r1.DesviacionCalificacion;
            //var ran_2 = r1.PromedioCalificacion - r1.MitadDesviacionCalificacion;
            //var ran_3 = r1.PromedioCalificacion + r1.MitadDesviacionCalificacion; 
            //var ran_4 = r1.PromedioCalificacion + r1.MitadDesviacionCalificacion + r1.DesviacionCalificacion;

            var ran_1 = 2;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion - r1.DesviacionCalificacion;
            var ran_2 = 5;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion;
            var ran_3 = 8;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion;
            var ran_4 = 9.5;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion + r1.DesviacionCalificacion;

            var res = new ViewModel_resultados_ejercicios()
            {
                Id = IdEstudiante,
                NumeroConceptosNodo = ListaNodos.Count,
                NumeroConceptosTrabajados = r != null ? r.NumeroConceptos.Value : 0,
                NumeroEjercicios = r != null ? r.NumeroEjercicios.Value : 0,

                SumatoriaPuntos = r != null ? r.TotalPuntos : -1,
                //SumatoriaCalificacionesFinal = r != null ? r.TotalCalificacionFinal : -1,
                //SumatoriaCalificacionesNoFinal = r != null ? r.TotalCalificacionNoFinal : -1,
                SumatoriaCalificaciones = r != null ? r.TotalCalificacion : -1,
                SumatoriaTiempos = r != null ? r.TotalTiempo : -1,

                PromedioCalificacionesNoFinal = r != null ? r.PromedioCalificacionNoFinal : -1,
                PromedioCalificacionesFinal = r != null ? r.PromedioCalificacionFinal : -1,
                PromedioCalificaciones = r != null ? r.PromedioCalificacion : -1,
                PromedioTiempos = r != null ? r.PromedioTiempo : -1,

                MayorCalificacion = r != null ? r.MayorCalificacion : -1,
                MenorCalificacion = r != null ? r.MenorCalificacion : -1,
                MayorTiempo = r != null ? r.MayorTiempo : -1,
                MenorTiempo = r != null ? r.MenorTiempo : -1,

                DesviacionCalificacion = r != null ? r.DesviacionCalificacion : -1,
                DesviacionTiempos = r != null ? r.DesviacionTiempo : -1,

                //PromedioGeneral = r1 != null ? r1.PromedioCalificacion : -1,
                //DesviacionCalificacionGeneral = r1 != null ? r1.DesviacionCalificacion : -1,
                //DesviacionTiempoGeneral = r1 != null ? r1.DesviacionTiempo : -1,
                //MitadDesviacionCalificacionGeneral = r1 != null ? r1.MitadDesviacionCalificacion : -1,
                //MitadDesviacionTiempoGeneral = r1 != null ? r1.MitadDesviacionTiempo : -1,

                Estrellas = r == null || r.NumeroEjercicios == 0 ? 0 : r.PromedioCalificacion > ran_4 ? 5
                        : r.PromedioCalificacion > ran_3 ? 4
                        : r.PromedioCalificacion > ran_2 ? 3
                        : r.PromedioCalificacion > ran_1 ? 2
                        : r.PromedioCalificacion > 0 ? 1 : 0,

                FechaFinal = fechafinal,
                FechaInicio = fechainicio,


            };

            return res;
        }

        public static ViewModel_resultados_ejercicios CalcularInformacionGrupoConcepto_ResumenAcumulado(List<ViewModel_nodo> ListaNodos, Guid IdEstudiante, DateTime fechainicio, DateTime fechafinal)
        {
            var r = new sofia_grupo_ObtenerInformacionListaConceptos_ResumenDiarioAcumulado_Result();

            using (var db = new math_reportesEntities())
            {
                fechafinal = db.Log_procesamiento.Where(x => x.Tipo == 9).Max(x => x.FechaProcesadaInicio);
            }

            fechainicio = fechainicio > fechafinal ? fechafinal : fechainicio;
            var l_conceptos = string.Join(",", ListaNodos.Select(x => x.properties.uuid.ToString()));


            using (var db = new math_reportesEntities())
            {
                r = db.sofia_grupo_ObtenerInformacionListaConceptos_ResumenDiarioAcumulado(fechainicio, fechafinal, IdEstudiante, l_conceptos).FirstOrDefault();
            }

            var ran_1 = 2;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion - r1.DesviacionCalificacion;
            var ran_2 = 5;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion;
            var ran_3 = 8;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion;
            var ran_4 = 9.5;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion + r1.DesviacionCalificacion;


            var res = new ViewModel_resultados_ejercicios()
            {
                Id = IdEstudiante,
                NumeroConceptosNodo = ListaNodos.Count,
                NumeroConceptosTrabajados= r != null ? r.NumeroConceptos.Value : 0,
                NumeroEjercicios = r != null ? r.NumeroEjercicios.Value : 0,

                SumatoriaPuntos = r != null ? r.TotalPuntos : -1,
                //SumatoriaCalificacionesFinal = r != null ? r.TotalCalificacionFinal : -1,
                //SumatoriaCalificacionesNoFinal = r != null ? r.TotalCalificacionNoFinal : -1,
                SumatoriaCalificaciones = r != null ? r.TotalCalificacion : -1,
                SumatoriaTiempos = r != null ? r.TotalTiempo : -1,

                PromedioCalificacionesNoFinal = r != null ? r.PromedioCalificacionNoFinal : -1,
                PromedioCalificacionesFinal = r != null ? r.PromedioCalificacionFinal : -1,
                PromedioCalificaciones = r != null ? r.PromedioCalificacion : -1,
                PromedioTiempos = r != null ? r.PromedioTiempo : -1,

                MayorCalificacion = r != null ? r.MayorCalificacion : -1,
                MenorCalificacion = r != null ? r.MenorCalificacion : -1,
                MayorTiempo = r != null ? r.MayorTiempo : -1,
                MenorTiempo = r != null ? r.MenorTiempo : -1,

                DesviacionCalificacion = r != null ? r.DesviacionCalificacion : -1,
                DesviacionTiempos = r != null ? r.DesviacionTiempo : -1,

                Estrellas = r == null || r.NumeroEjercicios == 0 ? 0 : r.PromedioCalificacion > ran_4 ? 5
                        : r.PromedioCalificacion > ran_3 ? 4
                        : r.PromedioCalificacion > ran_2 ? 3
                        : r.PromedioCalificacion > ran_1 ? 2
                        : r.PromedioCalificacion > 0 ? 1 : 0,

                FechaFinal = fechafinal,
                FechaInicio = fechainicio,
            };

            return res;
        }

        public static ViewModel_resultados_ejercicios CalcularInformacionEscuelaConcepto_ResumenAcumulado(List<ViewModel_nodo> ListaNodos, Guid IdEscuela,  DateTime fechainicio, DateTime  fechafinal)
        {
            var r = new sofia_escuela_ObtenerInformacionListaConceptos_ResumenDiarioAcumulado_Result();

            using (var db = new math_reportesEntities())
            {
                fechafinal = db.Log_procesamiento.Where(x => x.Tipo == 9).Max(x => x.FechaProcesadaInicio);
            }

            fechainicio = fechainicio > fechafinal ? fechafinal : fechainicio;
            var l_conceptos = string.Join(",", ListaNodos.Select(x => x.properties.uuid.ToString()));


            using (var db = new math_reportesEntities())
            {
                r = db.sofia_escuela_ObtenerInformacionListaConceptos_ResumenDiarioAcumulado(fechainicio, fechafinal, IdEscuela, l_conceptos).FirstOrDefault();
            }

            var ran_1 = 2;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion - r1.DesviacionCalificacion;
            var ran_2 = 5;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion;
            var ran_3 = 8;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion;
            var ran_4 = 9.5;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion + r1.DesviacionCalificacion;


            var res = new ViewModel_resultados_ejercicios()
            {
                Id = IdEscuela,
                NumeroConceptosNodo = ListaNodos.Count,
                NumeroConceptosTrabajados= r != null ? r.NumeroConceptos.Value : 0,
                NumeroEjercicios = r != null ? r.NumeroEjercicios.Value : 0,

                SumatoriaPuntos = r != null ? r.TotalPuntos : -1,
                //SumatoriaCalificacionesFinal = r != null ? r.TotalCalificacionFinal : -1,
                //SumatoriaCalificacionesNoFinal = r != null ? r.TotalCalificacionNoFinal : -1,
                SumatoriaCalificaciones = r != null ? r.TotalCalificacion : -1,
                SumatoriaTiempos = r != null ? r.TotalTiempo : -1,

                PromedioCalificacionesNoFinal = r != null ? r.PromedioCalificacionNoFinal : -1,
                PromedioCalificacionesFinal = r != null ? r.PromedioCalificacionFinal : -1,
                PromedioCalificaciones = r != null ? r.PromedioCalificacion : -1,
                PromedioTiempos = r != null ? r.PromedioTiempo : -1,

                MayorCalificacion = r != null ? r.MayorCalificacion : -1,
                MenorCalificacion = r != null ? r.MenorCalificacion : -1,
                MayorTiempo = r != null ? r.MayorTiempo : -1,
                MenorTiempo = r != null ? r.MenorTiempo : -1,

                DesviacionCalificacion = r != null ? r.DesviacionCalificacion : -1,
                DesviacionTiempos = r != null ? r.DesviacionTiempo : -1,

                Estrellas = r == null ||  r.NumeroEjercicios == 0 ? 0 : r.PromedioCalificacion > ran_4 ? 5
                        : r.PromedioCalificacion > ran_3 ? 4
                        : r.PromedioCalificacion > ran_2 ? 3
                        : r.PromedioCalificacion > ran_1 ? 2
                        : r.PromedioCalificacion > 0 ? 1 : 0,

                FechaFinal = fechafinal,
                FechaInicio = fechainicio,

            };

            return res;
        }


        public static List< ViewModel_resultados_ejercicios> CalcularInformacionDistribuidorConcepto_ResumenAcumulado(List<ViewModel_nodo> ListaNodos, Guid IdDistribuidor, DateTime fechainicio, DateTime fechafinal)
        {
           List<sofia_grupo_ObtenerInformacionListaConceptosEscuelas_ResumenDiarioAcumulado_Result>  rr = new List<sofia_grupo_ObtenerInformacionListaConceptosEscuelas_ResumenDiarioAcumulado_Result>();

            using (var db = new math_reportesEntities())
            {
                fechafinal = db.Log_procesamiento.Where(x => x.Tipo == 9).Max(x => x.FechaProcesadaInicio);
            }

            fechainicio = fechainicio > fechafinal ? fechafinal : fechainicio;
            var l_conceptos = string.Join(",", ListaNodos.Select(x => x.properties.uuid.ToString()));


            using (var db = new math_reportesEntities())
            {
                rr = db.sofia_grupo_ObtenerInformacionListaConceptosEscuelas_ResumenDiarioAcumulado(fechainicio, fechafinal, IdDistribuidor, l_conceptos).ToList();
            }

            var ran_1 = 2;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion - r1.DesviacionCalificacion;
            var ran_2 = 5;//r1.PromedioCalificacion - r1.MitadDesviacionCalificacion;
            var ran_3 = 8;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion;
            var ran_4 = 9.5;//r1.PromedioCalificacion + r1.MitadDesviacionCalificacion + r1.DesviacionCalificacion;

            List<ViewModel_resultados_ejercicios> res = new List<ViewModel_resultados_ejercicios>();

            foreach (var r in rr)
            {
                var rest = new ViewModel_resultados_ejercicios()
                {
                    Id = IdDistribuidor,
                    NumeroConceptosNodo = ListaNodos.Count,
                    NumeroConceptosTrabajados = r != null ? r.NumeroConceptos.Value : 0,
                    NumeroEjercicios = r != null ? r.NumeroEjercicios.Value : 0,

                    SumatoriaPuntos = r != null ? r.TotalPuntos : -1,
                    //SumatoriaCalificacionesFinal = r != null ? r.TotalCalificacionFinal : -1,
                    //SumatoriaCalificacionesNoFinal = r != null ? r.TotalCalificacionNoFinal : -1,
                    SumatoriaCalificaciones = r != null ? r.TotalCalificacion : -1,
                    SumatoriaTiempos = r != null ? r.TotalTiempo : -1,

                    PromedioCalificacionesNoFinal = r != null ? r.PromedioCalificacionNoFinal : -1,
                    PromedioCalificacionesFinal = r != null ? r.PromedioCalificacionFinal : -1,
                    PromedioCalificaciones = r != null ? r.PromedioCalificacion : -1,
                    PromedioTiempos = r != null ? r.PromedioTiempo : -1,

                    MayorCalificacion = r != null ? r.MayorCalificacion : -1,
                    MenorCalificacion = r != null ? r.MenorCalificacion : -1,
                    MayorTiempo = r != null ? r.MayorTiempo : -1,
                    MenorTiempo = r != null ? r.MenorTiempo : -1,

                    DesviacionCalificacion = r != null ? r.DesviacionCalificacion : -1,
                    DesviacionTiempos = r != null ? r.DesviacionTiempo : -1,

                    Estrellas = r == null || r.NumeroEjercicios == 0 ? 0 : r.PromedioCalificacion > ran_4 ? 5
                            : r.PromedioCalificacion > ran_3 ? 4
                            : r.PromedioCalificacion > ran_2 ? 3
                            : r.PromedioCalificacion > ran_1 ? 2
                            : r.PromedioCalificacion > 0 ? 1 : 0,

                    FechaFinal = fechafinal,
                    FechaInicio = fechainicio,

                };

                res.Add(rest);
            }
           




            return res;
        }

        #endregion


        #region Resumen concepto general


        public static ViewModel_resultados_estudiante_concepto_general CalcularInformacionEstudianteConceptoGeneral_ResumenAcumulado(Guid IdEstudiante, DateTime fechainicio, DateTime fechafinal)
        {
            var rr = new List<ViewModel_resultados_ejercicios_conceptos>();
            var rrr = new ViewModel_resultados_estudiante_concepto_general();

            using (var db = new math_reportesEntities())
            {
                fechafinal = db.Log_procesamiento.Where(x => x.Tipo == 8).Max(x => x.FechaProcesadaInicio);
            }

            fechainicio = fechainicio > fechafinal ? fechafinal : fechainicio;

            using (var db = new math_reportesEntities())
            {
                rr = db.sofia_estudiante_ObtenerInformacionConceptosGeneral_ResumenDiarioAcumulado(fechainicio, fechafinal, IdEstudiante)
                    .Select(r => new ViewModel_resultados_ejercicios_conceptos()
                    {
                        Id = IdEstudiante,
                        IdConcepto = r != null ? r.IdConcepto : Guid.Empty,
                        NumeroConceptosTrabajados = r != null ? r.NumeroConceptos.Value : 0,
                        NumeroEjercicios = r != null ? r.NumeroEjercicios.Value : 0,

                        SumatoriaPuntos = r != null ? r.TotalPuntos : -1,
                        //SumatoriaCalificacionesFinal = r != null ? r.TotalCalificacionFinal : -1,
                        //SumatoriaCalificacionesNoFinal = r != null ? r.TotalCalificacionNoFinal : -1,
                        SumatoriaCalificaciones = r != null ? r.TotalCalificacion : -1,
                        SumatoriaTiempos = r != null ? r.TotalTiempo : -1,

                        PromedioCalificacionesNoFinal = r != null ? r.PromedioCalificacionNoFinal : -1,
                        PromedioCalificacionesFinal = r != null ? r.PromedioCalificacionFinal : -1,
                        PromedioCalificaciones = r != null ? r.PromedioCalificacion : -1,
                        PromedioTiempos = r != null ? r.PromedioTiempo : -1,

                        MayorCalificacion = r != null ? r.MayorCalificacion : -1,
                        MenorCalificacion = r != null ? r.MenorCalificacion : -1,
                        MayorTiempo = r != null ? r.MayorTiempo : -1,
                        MenorTiempo = r != null ? r.MenorTiempo : -1,

                        DesviacionCalificacion = r != null ? r.DesviacionCalificacion : -1,
                        DesviacionTiempos = r != null ? r.DesviacionTiempo : -1,

                        FechaFinal = fechafinal,
                        FechaInicio = fechainicio,
                    })
                    .ToList();
            }


            rrr.ListaConceptos = rr;

            return rrr;
        }

        #endregion


        #region Resumen actividades

        public static ViewModel_resultados_actividades ObtenerResumenActividadesGrupo(Guid IdGrupo, DateTime fechainicio, DateTime fechafinal)
        {
            var r = new sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado_Result();

            using (var db = new math_reportesEntities())
            {
                fechafinal = db.Log_procesamiento.Where(x => x.Tipo == 14).Max(x => x.FechaProcesadaInicio);
                
            }

            fechainicio = fechainicio > fechafinal ? fechafinal : fechainicio;

            using (var db = new math_reportesEntities())
            {
                r = db.sofia_grupo_ObtenerInformacionActividades_ResumenDiarioAcumulado(fechainicio, fechafinal, IdGrupo).FirstOrDefault();
            }

            var res = new ViewModel_resultados_actividades()
            {

                Id = IdGrupo,
                DiasConsultados = r != null ? r.DiasConsultados.Value : -1,
                DiasActividad = r != null ? r.DiasActividad.Value : -1,
                ActividadesRegistradas =  r != null ? r.ActividadesRegistradas.Value : 0,
                ActividadesAsignadas =  r != null ? r.ActividadesAsignadas.Value : 0,

                NumeroEjerciciosActividadesAsignadas = r != null ? r.NumeroEjerciciosActividadesAsignadas.Value : -1,
                MaximoNumeroEjerciciosActividadesAsignadas = r != null ? r.MaximoNumeroEjerciciosActividadesAsignadas.Value : 0,
                MinimoNumeroEjerciciosActividadesAsignadas = r != null ? r.MinimoNumeroEjerciciosActividadesAsignadas.Value : 0,
                PromedioNumeroEjerciciosActividadesAsignadas = r != null ? r.PromedioNumeroEjerciciosActividadesAsignadas.Value : -1,

                NumeroEjerciciosActividadesRegistradas = r != null ? r.NumeroEjerciciosActividadesRegistradas.Value : -1,
                MaximoNumeroEjerciciosActividadesRegistradas = r != null ? r.MaximoNumeroEjerciciosActividadesRegistradas.Value : 0,
                MinimoNumeroEjerciciosActividadesRegistradas = r != null ? r.MinimoNumeroEjerciciosActividadesRegistradas.Value : 0,
                PromedioNumeroEjerciciosActividadesRegistradas = r != null ? r.PromedioNumeroEjerciciosActividadesRegistradas.Value : -1,

                FechaFinal = fechafinal,
                FechaInicio = fechainicio,
            };

            return res;
        }

        //public static ViewModel_resultados ObtenerResumenActividadesEscuela(Guid IdEscuela)
        //{
        //}

        #endregion
    }
}