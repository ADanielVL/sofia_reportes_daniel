//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SofiaWebReportes.Models
{
    using System;
    
    public partial class sofia_reporte_profesor_semaforo_Result
    {
        public System.Guid IdEstudiante { get; set; }
        public string Nombre { get; set; }
        public int TotalEjercicios { get; set; }
        public int TotalActividades { get; set; }
        public int TotalLibres { get; set; }
        public int TotalOtros { get; set; }
        public int TotalPuntos { get; set; }
        public double PromedioCalificacion { get; set; }
    }
}
