//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SofiaWebReportes.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class School_math
    {
        public System.Guid SchoolID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Type { get; set; }
        public int Scholar_Level { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string ClaveOficial { get; set; }
        public bool IsActive { get; set; }
        public string Detalle { get; set; }
        public bool IsFederal { get; set; }
        public bool IsPrueba { get; set; }
        public Nullable<System.Guid> IdUser { get; set; }
        public string Timezone { get; set; }
        public Nullable<int> TimezoneOffset { get; set; }
        public Nullable<int> Turno { get; set; }
        public Nullable<System.DateTime> FechaRegistro { get; set; }
        public Nullable<System.Guid> DistribuidorID { get; set; }
        public string Localidad { get; set; }
        public Nullable<System.Guid> IdATP { get; set; }
        public Nullable<System.Guid> IdZona { get; set; }
        public Nullable<System.Guid> IdSupervision { get; set; }
        public Nullable<bool> IsTiempoCompleto { get; set; }
        public Nullable<System.Guid> IdDirector { get; set; }
        public string Nip { get; set; }
        public Nullable<int> Dependencia { get; set; }
        public int EstadoCode { get; set; }
        public string FacRFC { get; set; }
        public string FacRazonSocial { get; set; }
        public string FacCalle { get; set; }
        public string FacNumExt { get; set; }
        public string FacNumInt { get; set; }
        public string FacColonia { get; set; }
        public string FacLocalidad { get; set; }
        public string FacReferencia { get; set; }
        public string FacDelegacion { get; set; }
        public Nullable<int> FacEstadoCode { get; set; }
        public string FacPais { get; set; }
        public Nullable<int> FacCodigoPostal { get; set; }
        public Nullable<System.Guid> IdImagen { get; set; }
        public string Subdominio { get; set; }
        public int NumAlum { get; set; }
        public Nullable<bool> IsChatEnabled { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public bool IsDistribuidor { get; set; }
        public bool IsComentariosMuroHabilitados { get; set; }
        public bool IsCCTValidado { get; set; }
        public bool IsInvitacion { get; set; }
        public Nullable<System.Guid> IdAsesor { get; set; }
        public System.Guid IdPais { get; set; }
        public System.Guid IdEstado { get; set; }
        public Nullable<System.Guid> IdCoordinador { get; set; }
        public string IdVinculacionExterna { get; set; }
        public int VinculacionExternaType { get; set; }
        public Nullable<decimal> Latitud { get; set; }
        public Nullable<decimal> Longitud { get; set; }
    }
}
