//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SofiaWebReportes.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Resumen_EstudianteEjercicios
    {
        public int Id { get; set; }
        public System.Guid IdEstudiante { get; set; }
        public System.DateTime FechaOperacion { get; set; }
        public int TotalEjercicios { get; set; }
        public int TotalEjerciciosAsignados { get; set; }
        public int TotalEjerciciosLibres { get; set; }
        public int TotalEjerciciosOtros { get; set; }
        public double TotalPuntos { get; set; }
        public double TotalCalificacion { get; set; }
        public int TotalTiempo { get; set; }
        public double PromedioPuntos { get; set; }
        public double PromedioCalificacion { get; set; }
        public double PromedioTiempo { get; set; }
        public Nullable<double> DesviacionCalificacion { get; set; }
        public Nullable<double> DesviacionTiempo { get; set; }
        public Nullable<double> MayorCalificacion { get; set; }
        public Nullable<double> MenorCalificacion { get; set; }
        public Nullable<double> MayorTiempo { get; set; }
        public Nullable<double> MenorTiempo { get; set; }
        public Nullable<double> SumatoriaCuadradosCalificacion { get; set; }
        public Nullable<double> SumatoriaCuadradosTiempo { get; set; }
        public int TimezoneOffset { get; set; }
        public Nullable<System.DateTime> FechaRegistro { get; set; }
    }
}
