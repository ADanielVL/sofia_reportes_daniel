﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SofiaWebReportes.Models
{

    
    public class ViewModel
    {
       
    }

    #region Nodos

    public class ViewModel_nodo
    {
        public int id { get; set; }
        public string name { get; set; }
        public ViewModel_properties_nodo properties { get; set; }

    }
    public class ViewModel_properties_nodo
    {
        public string parent_main_name { get; set; }
        public Guid uuid { get; set; } 
    }

    #endregion

    #region ViewModel resultados

    public class ViewModel_resultados_ejercicios
    {
        public Guid Id { get; set; }

        public int NumeroEjercicios { get; set; }
        public int NumeroConceptosNodo { get; set; }
        public int NumeroConceptosTrabajados { get; set; }

        public double? SumatoriaPuntos { get; set; }
        //public double? SumatoriaCalificacionesNoFinal { get; set; }
        //public double? SumatoriaCalificacionesFinal { get; set; }
        public double? SumatoriaCalificaciones { get; set; }
        public double? SumatoriaTiempos { get; set; }

        public double? PromedioCalificacionesNoFinal { get; set; }
        public double? PromedioCalificacionesFinal { get; set; }
        public double? PromedioCalificaciones { get; set; }
        public double? PromedioTiempos { get; set; }

        public double? MayorCalificacion { get; set; }
        public double? MenorCalificacion { get; set; }
        public double? MayorTiempo { get; set; }
        public double? MenorTiempo { get; set; }

        public double? DesviacionCalificacion { get; set; }
        public double? DesviacionTiempos { get; set; }


        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }

        public int TiempoConsultaNodos { get; set; }
        public int TiempoCalculoNodos { get; set; }
        public int TiempoRespuesta { get; set; }

        public double? PromedioGeneral { get; set; }
        public double? DesviacionCalificacionGeneral { get; set; }
        public double? DesviacionTiempoGeneral { get; set; }
        public double? MitadDesviacionCalificacionGeneral { get; set; }
        public double? MitadDesviacionTiempoGeneral { get; set; }

        public int Estrellas { get; set; }

    }

    public class ViewModel_resultados_ejercicios_conceptos
    {
        public Guid Id { get; set; }
        public Guid? IdConcepto { get; set; }

        public int NumeroEjercicios { get; set; }
        public int NumeroConceptosNodo { get; set; }
        public int NumeroConceptosTrabajados { get; set; }

        public double? SumatoriaPuntos { get; set; }
        public double? SumatoriaCalificaciones { get; set; }
        public double? SumatoriaTiempos { get; set; }

        public double? PromedioCalificacionesNoFinal { get; set; }
        public double? PromedioCalificacionesFinal { get; set; }
        public double? PromedioCalificaciones { get; set; }
        public double? PromedioTiempos { get; set; }

        public double? MayorCalificacion { get; set; }
        public double? MenorCalificacion { get; set; }
        public double? MayorTiempo { get; set; }
        public double? MenorTiempo { get; set; }

        public double? DesviacionCalificacion { get; set; }
        public double? DesviacionTiempos { get; set; }


        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }

        public int TiempoConsultaNodos { get; set; }
        public int TiempoCalculoNodos { get; set; }
        public int TiempoRespuesta { get; set; }

        public double? PromedioGeneral { get; set; }
        public double? DesviacionCalificacionGeneral { get; set; }
        public double? DesviacionTiempoGeneral { get; set; }
        public double? MitadDesviacionCalificacionGeneral { get; set; }
        public double? MitadDesviacionTiempoGeneral { get; set; }

        public int Estrellas { get; set; }

    }

    public class ViewModel_resultados_actividades
    {
        public Guid Id { get; set; }
        public int DiasConsultados { get; set; }
        public int DiasActividad { get; set; }
        public int ActividadesRegistradas { get; set; }
        public int ActividadesAsignadas { get; set; }

        public int NumeroEjerciciosActividadesAsignadas { get; set; }
        public int MaximoNumeroEjerciciosActividadesAsignadas { get; set; }
        public int MinimoNumeroEjerciciosActividadesAsignadas { get; set; }
        public double PromedioNumeroEjerciciosActividadesAsignadas { get; set; }

        public int NumeroEjerciciosActividadesRegistradas { get; set; }
        public int MaximoNumeroEjerciciosActividadesRegistradas { get; set; }
        public int MinimoNumeroEjerciciosActividadesRegistradas { get; set; }
        public double PromedioNumeroEjerciciosActividadesRegistradas { get; set; }

        public double DesviacionCalificacionActividadesCompletadas { get; set; }
        public double ActividadesCompletadas { get; set; }
        public double MaximaCalificacion { get; set; }
        public double MinimaCalificacion { get; set; }
        public int MaximoNumeroEstudiantesActividades { get; set; }
        public int MinimoNumeroEstudiantesActividades { get; set; }
        public int PromedioNumeroEstudiantesActividadesDiarios { get; set; }

        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }

        public int TiempoConsultaNodos { get; set; }
        public int TiempoCalculoNodos { get; set; }
        public int TiempoRespuesta { get; set; }

    }
    
    public class ViewModel_resultados_estudiante_concepto_general
    {
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }

        public int TiempoConsultaNodos { get; set; }
        public int TiempoCalculoNodos { get; set; }
        public int TiempoRespuesta { get; set; }

        public List<ViewModel_resultados_ejercicios_conceptos> ListaConceptos { get; set; }

    }

    #endregion
}