//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SofiaWebReportes.Models
{
    using System;
    
    public partial class sofia_reporte_semaforo_Escuela_Result
    {
        public Nullable<System.DateTime> FechaInicio { get; set; }
        public Nullable<System.DateTime> FechaFinal { get; set; }
        public string Ciudad { get; set; }
        public string NombreEscuela { get; set; }
        public string CCT { get; set; }
        public string Escolaridad { get; set; }
        public string Turno { get; set; }
        public Nullable<int> Actividades_Asignadas { get; set; }
        public int TotalEjercicios { get; set; }
        public int EjerciciosActividades { get; set; }
        public int EjerciciosLibres { get; set; }
        public int EjerciciosOtros { get; set; }
        public Nullable<int> Profesores_con_Grupo { get; set; }
        public Nullable<int> Estudiantes_Grupo { get; set; }
        public Nullable<int> LicenciaAlumno { get; set; }
    }
}
